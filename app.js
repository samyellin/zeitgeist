var restify = require('restify');
var builder = require('botbuilder');
var unirest = require('unirest');
//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Create chat bot
var connector = new builder.ChatConnector({
    appId: "b1ef243f-2eb9-4c0d-9314-66296e4e062b",
    appPassword: "tcjeTa5gcuRC9PkBJacSBfn"
});
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Bots Dialogs
//=========================================================

bot.dialog('/', [
    var story = new String;
    function (session) {
        builder.Prompts.text(session,'Hello! Please tell us anything on your mind.  Good or bad, happy or sad, we want to hear.  The goal is to visualize the way the world feels at this very moment.');
    },
    function (session, results) {
        if (results.response) {
            unirest.post("https://japerk-text-processing.p.mashape.com/sentiment/")
                .header("X-Mashape-Key", "5RPHc8NalimshAHCeT5OFX6SLEBep1L6pkcjsnoVAzECZPseHV")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "application/json")
                .send("language=english")
                .send("text=%s", results.response)
                .end(function (result) {
                    story = result.body;
                });
            session.send(story);
        }
    }
]);
